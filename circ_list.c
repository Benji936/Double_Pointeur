#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "circ_list.h"

void display(Element* head){

    Element* now = head;
    printf("[");

    do {
        printf("%d;",now->content);
        now = now->next;
        if(now == NULL){
            break;
        }
    }while(now->part != Head);
    printf("]");
}


//Fonction Créant des éléments par défaut
Element* element_create(void *data, Body Part, Element* Next){

    //Allocation de la mémoire
    Element* New = calloc(1,sizeof(Element));
    //Attribution des données
    New->content = data;
    New->part = Part;
    New->next = Next;
    //Retour de l'élément
    return New;
}

//Fonction qui créer une liste vide contenant seulement la tête
Element* list_create(){

   Element* create = element_create(NULL,Head,NULL);
   return create;
}

//Fonction Créant un chaine par défaut
Chain_Circ* chain_create(){

    //Déclaration de la tête ainsi de que de la queue
    Element* tail = element_create(NULL,Tail,NULL);
    //last et next correspondent a la queue pour la tête
    Element* head = element_create(NULL,Head,tail);
    //et inversement pour la queue
    tail->next = head;
    //Attribution de la mémoire pour la chaine
    Chain_Circ* New = calloc(1,sizeof(Chain_Circ));
    //Distribution de la tête, la queue et la longueur
    New->head = head;
    New->tail = tail;
    //2 est la longueur de base vue qu'il n'y a que la tête et la queue
    New->length = 2;

}

//Fonction vérifiant le contenu de la chaine
bool list_empty(Element* head){

    //Si head ne pointe sur rien alors la chaine est vide
    if(head->next == NULL) {
        //On retourne donc "vrai"
        return true;
    }
    else {
        //Sinon elle n'est pas vide on retourne "faux"
        return false;
    }
}

//Fonction qui déplace la tête au prochain élément
Element* list_move(Element* head){

    head->next->part= Head;
    Element* new_head = head->next;
    head->part = Tail;

    return new_head;
}

//Fonction qui compte le nombre d'élément de la liste en partant de la tête
unsigned int list_count(Element* head) {

    //On utilise une fonction qui contient la tête
    Element* now = head;
    //Et une autre qui va compter les éléments
    unsigned int count = 0;
    //On compte tant que l'on est pas de retour à la tête ou que le prochaine élément est NULL
    do{
        count++;
        //On passe à l'élément d'après ici
        now = now->next;
        if(now == NULL){
            break;
        }
    }while(now->part != Head);
    return count;
}

//Fonction qui insert un élément après la tête
Element* list_insert_after(Element* head, void* data){

    //On créer l'élément que l'on veut insérer
    Element* insert = element_create(data,Trunk,NULL);
    //Si il y avait déja un élément après la tête
    if(head->next != NULL){
        //Notre nouvel pointera dessus afin de garder intacte la liste
        insert->next = head->next;
    }
    //Sinon on fait simplement pointer la tête sur le nouvel élément
    head->next = insert;

    //Puis on le retourne
    return insert;

}

//Fonction qui insert un élément avant la tête
Element* list_insert_before(Element* head, void* data) {

    //On créé simplement un élément qui pointe sur la tête
    Element* insert = element_create(data,Trunk,head);

    return insert;
}

//Fonction de comparaison des données
int compare (void* data1, void* data2){

    //On vérifie si les valeurs sont différentes
    if (data1!=data2 ){

        return 0;
    }
    //Si la condition n'est pas remplie alors elle sont égales
    else {

        return 1;
    }
}

//Fonction qui recherche un élément dans la liste
Element* list_search(Element* head, void* data, int (*compare)(void*, void*)){

    //On stock head dans une variable
    Element* now = head;

    //Boucle s'arrêtant au retour de la tête si c'est une liste circulaire ou des qu'un élément est NULL
    do{
        //On compare les valeurs
        int v = (*compare)(data,now->content);
        //Si c'est 1 on retourne l'élément car on l'a trouvé
        if(v==1){
         return now;
        }
        //Sinon on passe à celui d'après
        now = now->next;

        if(now == NULL){
            return NULL;
        }

    }while(now->part != Head);

    //Si on fini la boucle sans rien avoir trouvé on renvoi NULL
    return NULL;
}


//Fonction qui supprime un élément dans la liste
Element* list_remove(Element* head, void* data, int (*compare)(void*, void*)){

    //On stock head dans une variable
    Element* now = head;
    //On créé une variable pour garder l'élément précédent
    Element* last;

    //Boucle s'arrêtant au retour de la tête si c'est une liste circulaire ou des qu'un élément est NULL
    do{
        //On compare les valeurs
        int v = (*compare)(data,now->content);
        //Si le retour est 1
        if(v==1){
            //on fait pointé l'élément juste avant celui qu'on supprime à l'élément après celui-ci
            last->next = now->next;

            //Et l'élément qu'on veut enlever de la liste ne pointe plus sur rien. De cette façon il n'est plus dans la liste
            now->next = NULL;
            return now;
        }

        //le précédent de vient l'actuel
        last = now;
        //l'actuel devient le prochain
        now = now->next;

        if(now == NULL){
            return NULL;
        }

    }while(now->part != Head);

    //Si on fini la boucle sans rien avoir trouvé on renvoi NULL
    return NULL;

}


void list_free(Element* head, void (*data_free)(void*)){

    //On stock head dans une variable
    Element* now = head;

    //Boucle s'arrêtant au retour de la tête si c'est une liste circulaire ou des qu'un élément est NULL
    do{

        (*data_free)(now);

        //l'actuel devient le prochain
        now = now->next;

        if(now->next == NULL){
            break;
        }

    }while(now->part != Head);

}

//TESTS//

//TEST DE LA CREATION DE CHAINE
void Test1() {

    //On créer un chaine afin de vérifier si elle correspond bien à nôtre code
    Chain_Circ* test = chain_create();

    //On verifie chaque attribut pour qu'il correspondent bien au code
    if(test->length == 2 && test->head->part == Head && test->tail->part == Tail){
        printf("\nTest chain_create() OK");
    }
        //Si la condition n'est pas rempli alors notre fonction ne fait pas ce que l'on veut
    else{
        printf("\nTest chain_create() Error");
    }

    //On libère la mémoire
    free(test->head);
    free(test->tail);
    free(test);

}

//TEST COMPTE DE LA LISTE
void Test2(){

    //Création d'une chaine par défaut
    Chain_Circ* test = chain_create();

    //On verifie si ça correspond a la taille donnée par défaut
    if(list_count(test->head)== test->length){
        printf("\nTest list_count() OK");
    }
    else{
        printf("\nTest list_count() Error");
    }

    //Libération de la mémoire
    free(test->head);
    free(test->tail);
    free(test);
}

//TEST INSERTION AVANT ET APRES LA TETE
void Test4() {

    //On créer une liste
    Element* head = list_create();
    int* data = 1;

    //Utilisation de la fonction d'insertion après la tête
    Element* insert1 = list_insert_after(head, data);
    //Si la tête pointe bien sur l'élément retourné et que la donné est bien celle que l'on voulait le test est bon
    if(head->next == insert1 && head->next->content == data){
        printf("\nTest list_insert_after() OK");
    }
    else {
        printf("\nTest list_insert_after() Error");
    }

    //Puis on refait la même chose avec la fonction d'insertion avant la tête
    Element* insert2 = list_insert_before(head,data);
    if(insert2->next == head && insert2->content == data){
        printf("\nTest list_insert_before() OK");
    }
    else {
        printf("\nTest list_insert_before() Error");
    }
    list_free(head,free);
}

//TEST DE LA FONCTION DE RECHERCHE
void Test5(){

    //Création d'un liste
    Element* test = list_create();
    //Variable de la donnée qui sera testée
    int* data;
    //Insertion d'élément de 5 à 1
    for(int i = 5; i>0; i--){
        data = i;
        list_insert_after(test,data);
    }
    //Recherche d'un des éléments inséré
    data = 3;
    Element* test_v = list_search(test,data,compare);

    //Recherche d'un élément qui n'a pas été inséré
    data = 7;
    Element* test_f = list_search(test,data,compare);

    //Si il trouve le premier et non le deuxieme alors la fonction marche parfaitement
    if(test_v != NULL && test_f == NULL){
        printf("\nTest list_search() OK");
    }
    else{
        printf("\nTest list_search() Error");
    }
    //On libére la mémoire
    list_free(test,free);
}

//TEST DE LA FONCTION DE SUPPRESSION D'UN ELEMENT DE LA LISTE
void Test6(){

    //Création d'un liste
    Element* test = list_create();
    //Variable de la donnée qui sera testée
    int* data;
    //Insertion d'élément de 5 à 1
    for(int i = 5; i>0; i--){
        data = i;
        list_insert_after(test,data);
    }
    data = 3;
    list_remove(test,data,compare);
    Element* test_s = list_search(test,data,compare);

    if (test_s == NULL){
        printf("\nTest list_remove() OK");
    }
    else{
        printf("\nTest list_remove() Error");
    }
    //On libère la mémoire
    list_free(test,free);
}

//Fonction de tout les Tests
void All_Test(){
    Test1();
    Test2();

    Test4();
    Test5();
    Test6();
}