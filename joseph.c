#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "circ_list.h"

//Fonction qui supprime le k-eme élément
Element* delete_k(Element* head,int k){

    //Variable de déplacement dans la liste
    Element* now = head;
    //variable pour le compte
    int loop = 1;
    //Si k = 1 cela veut dire que l'on va supprimer la tête donc on la déplace
    if(k==1){
        head = list_move(head);
    }
    //Boucle jusqu'a que que notre compte soit égale à k
    while(loop!=k){
        //Incrémentation
        loop++;
        //On passe à l'élément d'après
        now = now->next;
    }
    //Affichage de l'élément supprimé comme demandé
    printf("\n%d",now->content);
    //Suppression
    list_remove(head,now->content,compare);
    //On retourne la tête dans le cas où elle aurait été changé
    return head;
}

int main (int argc, char *argv[]){

    //On récupère les 2 premiers arguments
    int n = atoi(argv[1]);
    int k = atoi(argv[2]);

    //Création d'une liste
    Element* head = list_create();
    //On donne la valeur 1
    head->content = 1;

    int* kk;
    //Boucle d'insertion jusqu'a avoir n élément dans la liste chainée
    for(int i=n; i>1; i--) {
        kk = i;
        list_insert_after(head,kk);
    }

    int* length = n;
    //On cherche le dernier élément de la liste
    Element* last = list_search(head,length,compare);
    //On le fait pointé sur la tête pour avoir réellement un liste chainée
    last->next = head;

    //Affichage de la liste
    display(head);
    //Boucle pour l'ordre de suprression
    for(int i =0; i<n; i++){

        //Si "k" depasse "n" on lui soustrait "n" et on lui ajoute "i"(qui correspond au nombre d'éléments supprimés)
        if(k>n){
            k = k-n+i;
        }
        //On supprime l'élément à la k-eme place moins i
        head = delete_k(head,k-i);
        //On augmente k de sa valeur de départ a chaque boucle
        k+=atoi(argv[2]);

    }
}