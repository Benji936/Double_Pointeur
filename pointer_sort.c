#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

//Fonction de Vérification d'une valeur dans un tableau
int check(int* tab[],unsigned int len, int n){

    //On parcours
    for(int i=0; i<len; i++){
        //Si la valeur est égale à une d'une tableau on retourne 1
        if(n==tab[i]){
            return 1;
        }
    }
    //Sinon 0
    return 0;
}

//Fonction de sort
int* sort(int tab[],unsigned int len){

    //Tableau de pointeur(len-1 car on prendra argc en argument)
    int* ptr_tab[len-1];
    //variable utile dans le processus
    int sort;

    //Double boucle
    for(int j=0; j<len-1; j++) {
        //Initialisation
        sort = 0;

        for (int i = 0; i < len-1; i++) {
            //On vérifie d'abord si la valeur n'est pas déjà dans le tableau autrement on la passe
            int v = check(ptr_tab,len-1,tab[i]);
            if (v == 1) {
                continue;
            }
            //Sort prend la première valeur valable du tableau
            if(sort==0){sort = tab[i];}

            //Ensuite on compare avec chaque élément pour voir lequel est le plus petit
            if (sort >= tab[i]) {

                //On pointe sur celui ci tant qu'on en trouve pas un autre encore plus petit
                ptr_tab[j] = *(tab+i);
                //Si on trouve un plus petit on remplace les valeurs
                sort = tab[i];
            }
        }
        //Affichage des valeurs
        printf("\n%d",ptr_tab[j]);
    }
    return ptr_tab;
}


int main(int argc, char *argv[]){

    //Tableau qui contiendra les arguments
    int tab[argc];

    //Boucle (on commence à 1 pour ne pas prendre le nom du fichier dans nos arguments)
    for(int i=1; i<argc; i++){
        //On transforme les char en int et on les entre dans le tableau
        tab[i-1]=atoi(argv[i]);
    }
    //On sort les valeurs
    int* ptr = sort(tab,argc);


}