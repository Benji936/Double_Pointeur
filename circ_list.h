#include <stdbool.h>
//Enum Body afin de déterminer les partis de la chaine dans nôtre structure Element suivante
typedef enum Body {

    Head,
    Tail,
    Trunk,

}Body;

//Structure Element Contenant la donnée, l'élément suivant, l'élément précédent et la parti de la chaine (queue, tête ou tronc)
typedef struct Element{

    void* content;
    struct Element* next;
    Body part;

}Element;

//Structure Représentant une chaine circulaire composé de la tête et de la queue ainsi que de la longeur de la chaine
typedef struct Chain_Circ {

    Element* head;
    Element* tail;
    unsigned int length;

}Chain_Circ;

//Fonctions defini pour le fichier "circ_list.c"

Element* element_create(void *data, Body Part, Element* Next);

Element* list_create();

Chain_Circ* chain_create();

bool list_empty(Element* head);

Element* list_move(Element* head);

unsigned int list_count(Element* head);

Element* list_insert_after(Element* head, void* data);

Element* list_insert_before(Element* head, void* data);

int compare(void* data1, void* data2);

Element* list_search(Element* head, void* data, int (*compare)(void*, void*));

Element* list_remove(Element* head, void* data, int (*compare)(void*, void*));

void list_free(Element* head, void (*data_free)(void*));

void display(Element* head);


