All: joseph matrix_compute pointer_sort

joseph: circ_list.o joseph.o
	gcc -o joseph circ_list.o joseph.o

matrix_compute: matrix.o matrix_compute.o
	gcc -o matrix_compute matrix.o matrix_compute.o

pointer_sort: pointer_sort.o
	gcc -o pointer_sort pointer_sort.o

circ_list.o: circ_list.c
	gcc -o circ_list.o -c circ_list.c

joseph.o: joseph.c
	gcc -o joseph.o -c joseph.c

matrix.o: matrix.c
	gcc -o matrix.o -c matrix.c

matrix_compute.o: matrix_compute.c
	gcc -o matrix_compute.o -c matrix_compute.c


pointer_sort.o: pointer_sort.c
	gcc -o pointer_sort.o -c pointer_sort.c

clean:
	del /f /q *.o
